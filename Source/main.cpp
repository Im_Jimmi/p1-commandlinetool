//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <stdlib.h>

int factorial(int input)
{
	if (input < 0) {
		std::cout << "error. input was negative!" << std::endl;
		return -1;
	}

	int iOut = input;

	for (int i = input - 1; i > 0; i--)
		iOut *= i;

	return iOut;
}

int main (int argc, const char* argv[])
{
	int iNum, iOut;

	std::cin >> iNum;
	std::cout << std::endl;

	iOut = factorial(iNum);
	if (iOut != -1)
		std::cout << iNum << "! = " << iOut << std::endl;

	system("PAUSE");
    return 0;
}